# Motif Desktop UWP

Motif is a tracker application for MOTivational Intermittent Fasting.


## Requirements

### Building

* .NET Framework v4.7.2
* Windows 10 SDK
  * Windows 10 App Development (UWP)
* Visual Studio Build Tools 2017
  * NuGet.exe
  * MSBuild.exe
    * MakeAppx.exe

### Testing

* VSTest (For run, it needs Visual Studio 2017 Installation)


## Build

```pwsh
> NuGet.exe restore "Motif/Motif.csproj"
> PowerShell.exe -ExecutionPolicy Bypass
    -File .\Build.ps1
    -Configuration Debug
    -Platform x64
    -Run
    -Clean
```

## Test

```pwsh
> NuGet.exe restore "Motif.Tests/Motif.Tests.csproj"
> PowerShell.exe -ExecutionPolicy Bypass
    -File .\RunTest.ps1
    -Platform x64
    -Clean
```

## Notes

### Logging

In **DEBUG**, a log file named `debug.log` will be created.

```bash
# on WSL
$ grc -c conf.debug tail -f /mnt/c/Users/<USER>/AppData/Local/Packages/ \
  <PACKAGE>/LocalCache/debug.log
$ ./taillog
```

If you want, check following `conf.debug` for grc.

```txt
regexp=^.*\s(TRACE)\s.*$
colours=unchanged,"\033[38;5;117m"
count=once
-
regexp=^.*\s(DEBUG)\s.*$
colours=unchanged,"\033[38;5;183m"
count=once
-
regexp=^.*\s(INFO)\s.*$
colours=unchanged,"\033[38;5;228m"
count=once
-
regexp=^.*\s(WARN)\s.*$
colours=unchanged,"\033[38;5;203m"
count=once
-
regexp=^.*\s(ERROR)\s.*$
colours=unchanged,"\033[38;5;199m"
count=once
-
regexp=^.*\s(FATAL)\s.*$
colours=unchanged,"\033[38;5;1m"
count=once
```


## License

`GPL-3.0`

```txt
Motif
Copyright (c) 2023 Yasuhiro Яша Asaka
```

See LICENSE.

```txt
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
