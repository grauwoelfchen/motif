format:
	@dotnet format --severity info --verify-no-changes test/Motif.Tests.sln
.PHONY: format
